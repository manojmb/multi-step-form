let currentStep = 1;
const steps = document.querySelectorAll(".stp");
const stp1 = document.querySelector(".stp-1");
const stp2 = document.querySelector(".stp-2");
const stp3 = document.querySelector(".stp-3");
const stp4 = document.querySelector(".stp-4");
const stp5 = document.querySelector(".stp-5");
const step1NxtBtn = document.querySelector(".stp-1 .next-btn");
const step2NxtBtn = document.querySelector(".stp-2 .next-btn");
const step3NxtBtn = document.querySelector(".stp-3 .next-btn");
const confirmBtn = document.querySelector(".stp-4 .next-btn");

const stp1Inputs = document.querySelectorAll(".stp-1 input");

document.addEventListener("DOMContentLoaded", function () {
  circleBackgroungChange();
  switchPrice(true);
});

function hideErrorForInput(event) {
  const textMissingError = event.target.previousElementSibling.lastElementChild;
  textMissingError.style.display = "none";
}

function checkInputs(event) {
  let nameRegex = /^[a-zA-Z]+$/;
  let success = true;
  stp1Inputs.forEach((inputElement) => {
    let textMissingError = inputElement.previousElementSibling.lastElementChild;
    textMissingError.innerText = "Please Fill out this field";
    if (inputElement.name === "emailaddress") {
      nameRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    }
    if (inputElement.name === "phone") {
      nameRegex = /^\d{10}$/;
    }
    if (!inputElement.value) {
      textMissingError.style.display = "block";
      success = false;
    } else if (!nameRegex.test(inputElement.value)) {
      textMissingError.innerText = "Please provide proper input";
      textMissingError.style.display = "block";
      success = false;
    } else {
      textMissingError.style.display = "none";
    }
  });
  return success;
}

step1NxtBtn.addEventListener("click", (event) => {
  event.preventDefault();
  if (checkInputs(event)) {
    stp1.classList.remove("stp-active");
    stp2.classList.add("stp-active");
    circleBackgroungChange();
    currentStep++;
  }
});

step2NxtBtn.addEventListener("click", (event) => {
  event.preventDefault();
  stp2.classList.remove("stp-active");
  stp3.classList.add("stp-active");
  circleBackgroungChange();
  currentStep++;
});

step3NxtBtn.addEventListener("click", (event) => {
  event.preventDefault();
  calculateTotal();
  stp3.classList.remove("stp-active");
  stp4.classList.add("stp-active");
  circleBackgroungChange();
  currentStep++;
  const changeBtn = document.querySelector(".change");
  changeBtn.addEventListener("click", (event) => {
    event.preventDefault();
    stp4.classList.remove("stp-active");
    stp2.classList.add("stp-active");
    circleBackgroungChange();
    currentStep -= 2;
  });
});

confirmBtn.addEventListener("click", (event) => {
  event.preventDefault();
  stp4.classList.remove("stp-active");
  stp5.classList.add("stp-active");
});

steps.forEach((step) => {
  const prevBtn = step.querySelector(".prev-stp");
  if (prevBtn) {
    prevBtn.addEventListener("click", (event) => {
      event.preventDefault();
      document
        .querySelector(`.stp-${currentStep}`)
        .classList.remove("stp-active");
      currentStep--;
      document.querySelector(`.stp-${currentStep}`).classList.add("stp-active");
      circleBackgroungChange();
    });
  }
});

function circleBackgroungChange() {
  const allSteps = document.querySelectorAll(".step .circle");
  allSteps.forEach((step) => {
    step.style.backgroundColor = "transparent";
    step.style.color = "hsl(229, 24%, 87%)";
  });
  const presentStep = document.querySelector(".stp-active").classList[1];
  if (presentStep) {
    const step = Number(presentStep.charAt(presentStep.length - 1));
    const presentCircle = document.querySelector(".step-" + step + " .circle");
    presentCircle.style.backgroundColor = "hsl(206, 94%, 87%)";
    presentCircle.style.color = "hsl(213, 96%, 18%)";
  }
}

const togglePlan = document.querySelector(".toggle-plan");
togglePlan.addEventListener("click", function () {
  const checked = document.querySelector("#check").checked;
  switchPrice(checked);
});

function switchPrice(checked) {
  const yearlyPrice = [90, 120, 150];
  const monthlyPrice = [9, 12, 15];
  const yearlyAddonPrice = [10, 20, 20];
  const monthlyAddonPrice = [1, 2, 2];
  const yearlyFree = document.querySelectorAll(".yearly-free");
  const prices = document.querySelectorAll(".plan-priced");
  const addonPrices = document.querySelectorAll(".addon-price");
  if (checked) {
    addonPrices[0].innerHTML = `+$${yearlyAddonPrice[0]}/yr`;
    addonPrices[1].innerHTML = `+$${yearlyAddonPrice[1]}/yr`;
    addonPrices[2].innerHTML = `+$${yearlyAddonPrice[2]}/yr`;
    yearlyFree.forEach((classP) => classP.classList.add("active"));
    prices[0].innerHTML = `$${yearlyPrice[0]}/yr`;
    prices[1].innerHTML = `$${yearlyPrice[1]}/yr`;
    prices[2].innerHTML = `$${yearlyPrice[2]}/yr`;
  } else {
    addonPrices[0].innerHTML = `+$${monthlyAddonPrice[0]}/mo`;
    addonPrices[1].innerHTML = `+$${monthlyAddonPrice[1]}/mo`;
    addonPrices[2].innerHTML = `+$${monthlyAddonPrice[2]}/mo`;
    yearlyFree.forEach((classP) => classP.classList.remove("active"));
    prices[0].innerHTML = `$${monthlyPrice[0]}/mo`;
    prices[1].innerHTML = `$${monthlyPrice[1]}/mo`;
    prices[2].innerHTML = `$${monthlyPrice[2]}/mo`;
  }
}

function calculateTotal() {
  const selectedPlanInput = document.querySelector(
    'input[name="billing-plan"]:checked'
  );
  const planName = selectedPlanInput.value;
  const planPrice =
    selectedPlanInput.parentElement.querySelector(".plan-priced").innerText;

  const selectedAddons = document.querySelectorAll(
    'input[name="addons"]:checked'
  );
  const selectedAddonsContainer = document.getElementById(
    "selectedAddonsContainer"
  );
  const selectedPlanContainer = document.getElementById("selectPlanContainer");

  let totalAmount = Number(planPrice.slice(1, -3));
  let planPeriod = planPrice.slice(-2);
  if (planPeriod === "yr") {
    planPeriod = "Yearly";
  } else {
    planPeriod = "Monthly";
  }

  selectedAddonsContainer.innerHTML = "";
  selectedPlanContainer.innerHTML = "";

  const selectedPlanDiv = createPlanElement(planName, planPrice, planPeriod);
  selectedPlanContainer.appendChild(selectedPlanDiv);

  selectedAddons.forEach((addon) => {
    const addonName =
      addon.nextElementSibling.querySelector("div p:first-child").innerText;
    const addonPrice =
      addon.nextElementSibling.querySelector(".addon-price").innerText;

    totalAmount += Number(addonPrice.slice(2, -3));

    const selectedAddonDiv = createAddonElement(addonName, addonPrice);

    selectedAddonsContainer.appendChild(selectedAddonDiv);
  });

  const totalAmountElement = document.getElementById("totalAmount");
  const totalPeriodElement = document.getElementById("per-period");
  totalAmountElement.textContent = `+$${totalAmount}${planPrice.slice(-3)}`;
  totalPeriodElement.textContent =
    planPeriod === "Monthly" ? `Total (per month)` : `Total (per year)`;
}

function createPlanElement(name, price, period) {
  const div = document.createElement("div");
  div.classList.add("selected-plan");
  div.innerHTML = `
  <div class="plan-and-change">
  <span class="plan-name">${name} (${period})</span>
  <button class="change">Change</button>
  </div>
    <span class="plan-price">${price}</span>
  `;
  return div;
}

function createAddonElement(name, price) {
  const div = document.createElement("div");
  div.classList.add("selected-addon");
  div.innerHTML = `
    <span class="service-name">${name}</span>
    <span class="service-price">${price}</span>
  `;
  return div;
}
